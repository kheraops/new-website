var hamburgerButton = document.getElementById('hamburger-menu');
var hamburgerButtonActive = document.getElementsByClassName('hamburger-button');
var navMobile = document.getElementById('nav-mobile');
var htmlTag = document.getElementsByTagName('html');

function toggleClass(element, className) {
    if (element.classList.contains(className)) {
        element.classList.remove(className);
    } else {
        element.classList.add(className);
    }
}

function removeClass(element, className) {
    element.classList.remove(className);
}

hamburgerButton.addEventListener('click', function(e) {
    e.preventDefault();
    toggleClass(hamburgerButtonActive[0], 'active');
    toggleClass(navMobile, 'open');
    toggleClass(htmlTag[0], 'overflow-hidden');
});

window.addEventListener("resize", function(){
    removeClass(hamburgerButtonActive[0], 'active');
    removeClass(navMobile, 'open');
    removeClass(htmlTag[0], 'overflow-hidden');
});
