## Add an industry to the our portfolio page

1. In the file `_date/our-portfolio.md` find the array `industries`
1. Add a industry in string format with double quotes around the industry like: `"new industry"`


Before:
```
industries: [
    "Industry"
]
```

After:
```
industries: [
    "Industry",
    "New industry"
]
```

**Important:** In the array `[]` every value should have a `,` behind the value (outside the quotes). 