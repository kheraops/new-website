## Adding a public report to the portfolio page

In the file `_data/our-portfolio.md` you can add/edit a public report.

1. Open the file and add/edit the value under the `public-reports:`
1. Copy the value shown below to the `public-reports`

**project**: The value of project can be HTML this way you can add. This value can be multiline. If it stays wrapped in `"` double quotes (see below)

**important**: Double quotes in the HTML need to be escaped by a backslash like: `<a href\"https://example.com\">` 

```
- client: "Mozilla"
  project: "Lorem ipsum dolor sit amet, 
           <a href=\"https://www.google.com\">consectetur</a> adipiscing elit. Nunc mattis massa eget diam fermentum, quis ultrices justo iaculis. Nunc tortor erat, efficitur a massa non, euismod interdum mi. Proin eleifend velit eu purus maximus porttitor. Proin ipsum diam, viverra sed sollicitudin at, mattis at nibh."
```