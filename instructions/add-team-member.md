## Add a team member to the team page

The team member pages can be found in the `_our-team` directory.
This directory can have 1 level of folders, these folders categorize the team members.

### How to add a new member to the team page
1. In one of the existing category folder or newly created folder/category in the `_our-team`
create a new `html` or `markdown (.md)` with the persons firstname and lastname as file name.
2. Fill in the front matter this is mandatory (see Front matter below)
3. Below the front matter add you content this can be HTML or Markdown.


#### Front matter
Copy the front matter (below) to the top of the html/markdown file and fill in the information.
```
---
firstName: Bart
lastName: Bartens
image: melanie.png
layout: team-member
title: Bart Bartens
order: 9999
---
```

Layout
---
The `layout` value cannot be changed. This is needed to render the page.

Order
---
The default order of the team page is alphabetical, but a custom order is possible.
If two team members have the same order, alphabetical is leading.

Image
---
You can add an image by adding the file name of the image to the `image` value in the front matter.
To add an image:
1. Place the image of the team member in the `assets/images/team` directory
2. Add the name of the file to the `image` value in the front matter. Only the filename is needed not the directory path.

**important:** Make sure the image is compressed (below **100kb**)
