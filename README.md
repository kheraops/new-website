# Radically Open Security Website

<https://www.radicallyopensecurity.com>

## Development

Serve

```sh
make serve
```

Build with relative paths

```sh
make build
```

Build standalone error pages with absolute urls

```sh
make build-error-pages
```
